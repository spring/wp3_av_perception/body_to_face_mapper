# FaceBodyCandidateMatches

## Topics used

Node [/body_to_face_mapper]
Publications: 
 * /humans/candidate_matches [hri_msgs/IdsMatch]
 * /rosout [rosgraph_msgs/Log]

Subscriptions: 
 * /humans/bodies/tracked [hri_msgs/IdsList]
 * /humans/faces/x/roi [unknown type]
 * /humans/bodies/x/roi [unknown type]
 * /humans/faces/tracked [hri_msgs/IdsList]
 * /tf [tf2_msgs/TFMessage]
 * /tf_static [tf2_msgs/TFMessage]

Services: 
 * /body_to_face_mapper/get_loggers
 * /body_to_face_mapper/set_logger_level
 * /body_to_face_mapper/tf2_frames



## Getting started

This projects is an implementation of a matcher between :
- faces detected on ari's head camera and published with ros4hri topics (/humans/faces/*)
- bodies detected on ari's torso fisheye camera and published with ros4hri topics (/humans/bodies/*)

it publishes ros4hri matches candidates

to start the rosnode, run : python3 src/facebodycandidatematches.py

A dockerfile is provided. For building, it needs hri_msgs/ and pyhri/ projects cloned first at the top of this project.
- build with : docker build -t facebodycandidatematches .
- to run with : docker run --rm -it --env ROS_MASTER_URI=http://192.168.0.10:11311 --env ROS_IP=192.168.0.1 -v /etc/hosts:/etc/hosts -v /local_scratch:/local_scratch facebodycandidatematches
