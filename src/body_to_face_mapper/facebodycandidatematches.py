import time
import numpy as np
from scipy.optimize import linear_sum_assignment

import cv2
from cv_bridge import CvBridge

import rospy
from sensor_msgs.msg import Image, CompressedImage, CameraInfo
from geometry_msgs.msg import PointStamped
import tf
import image_geometry
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus, KeyValue
from hri_msgs.msg import IdsMatch
import py_spring_hri


class FaceBodyMatcher:
    def __init__(self):
        #maximum distance for a match
        self.MAXDISTANCE = 200
        self.bridge = CvBridge()
        self.current_bodies = {}
        self.current_faces = {}
        self.current_faces_roi = {}
        self.current_faces_roi_fisheye = {}
        self.current_bodies_roi = {}

        self._publishers = []
        self._timers = []

        self.match_count = 0

        self.publish_matches = rospy.get_param("~DEBUG_DISPLAY", False)
        self.diag_timer_rate = rospy.get_param('~diag_timer_rate', 10)

        if self.publish_matches :
            print("Debugging enabled")
 
        self.tf_listener = tf.TransformListener()

        # get head camera geometric model and image size
        self.head_model = image_geometry.PinholeCameraModel()
        cam_info=rospy.wait_for_message('/head_front_camera/color/camera_info',CameraInfo)
        self.head_image_width=cam_info.width
        self.head_image_height=cam_info.height
        self.head_model.fromCameraInfo(cam_info)

        # print('head camera size', cam_info.width, cam_info.height)
        # /front_camera/fisheye/camera_info has width of 0 !
        #self.fisheye_image_width=cam_info.width


        # get fisheye camera geometric model and image size
        fisheye_data=rospy.wait_for_message('/front_fisheye_basestation/image_raw',Image)
        self.fisheye_image_width= fisheye_data.width
        self.fisheye_image_height= fisheye_data.height
        # fisheye camera calibration, provided by CWT
        self.fisheye_calib_width = 3264
        self.fisheye_calib_height= 2448
        self.fisheye_K = np.asarray([[  1188.69,     0.  ,  1631.948],
                    [    0.  ,   1189.301,   1242.818],
                    [    0.  ,     0.  ,     1.  ]])

        self.fisheye_D = np.array( [ 
                -0.065074864654844314,
                0.022948182911307041,
                -0.0039319897787193784,
                -0.0037411424922587362
            ])
        
        self.tf_listener = tf.TransformListener()

        self.hri_listener = py_spring_hri.HRIListener(
            face_properties = ['roi'],
            body_properties = ['roi'])

        self.hri_listener.add_body_property('cropped', msg_type=Image )
        self.hri_listener.add_face_property('cropped', msg_type=Image )

        if self.publish_matches :
            self.line_thickness= 4
            self._head_body_matches_pub = rospy.Publisher('/head_body_matches/image_raw/compressed', CompressedImage, queue_size=10)
            self._publishers.append(self._head_body_matches_pub)
        
        self.candidate_matches_publisher = rospy.Publisher('/humans/candidate_matches', IdsMatch, queue_size=10)
        self._publishers.append(self.candidate_matches_publisher)
        self._diagnostics_pub = rospy.Publisher('/diagnostics', DiagnosticArray, queue_size=10)
        self._publishers.append(self._diagnostics_pub)
        self._timers.append(rospy.Timer(rospy.Duration(1/self.diag_timer_rate), callback=self._diagnostics_callback))


    def compute_confidence(self, distance):
        # given the distance (in pixel) of a match, and the maximum distance allowed for a match,
        # compute a confidence indicator for this match
        if distance> self.MAXDISTANCE : return 0
        else : return (self.MAXDISTANCE-distance) / (self.MAXDISTANCE)


    def compute_matches_geom(self):
        start_time = time.time()

        rvec = np.array([[[0., 0., 0.]]])
        tvec = np.array([[[0., 0., 0.]]])

        n_faces=len(self.current_faces_roi)
        # print("{} Faces to match".format(n_faces))
        self.current_faces_roi_fisheye 
        use_hungarian = False
        self.match_count = 0
        if n_faces > 0 :
            n_bodies = len(self.current_bodies_roi)
            # if n_faces <= 1 or n_bodies <= 1then the problem is trivial
            if n_faces > 1 and n_bodies > 1 :
                use_hungarian = True
            image_matches = None            

            distances_matrix_flat=np.array([])
            #print("{}: {} faces and {} bodies to match".format(time.time(),len(self.current_faces), len(self.current_bodies)))
            for face_id, roi in self.current_faces_roi.items() :
                # for each face detected (in the head front camera) we first compute its ROI in the  fisheye frame              
                
                # faces rois are normalized (coordinates between 0 and 1), thus need to be multiplied by image size
                x_h = (roi.xmin + (roi.xmax - roi.xmin)/2) * self.head_image_width
                y_h = (roi.ymin + (roi.ymax - roi.ymin)/2 ) * self.head_image_height

                # use ros camera model to rectify the point and get a 3d unit vector 'ray' in the camera's frame 
                (u,v)= self.head_model.rectifyPoint((x_h,y_h))
                x,y,z=self.head_model.projectPixelTo3dRay((u,v))

                # transform this 3d point in fisheye camera frame
                target=PointStamped()
                target.header.frame_id = "/head_front_camera_color_optical_frame"
                target.header.stamp =rospy.Time(0)

                target.point.x=x
                target.point.y=y
                target.point.z=z
                try:
                    target_fish=self.tf_listener.transformPoint("/front_fisheye_camera_optical_frame",target)
                except Exception as e:
                    rospy.logwarn(e)
                    return
                objp = np.array([[[target_fish.point.x, target_fish.point.y, target_fish.point.z ]]])

                imgpoints, _ = cv2.fisheye.projectPoints(objp, rvec, tvec, self.fisheye_K, self.fisheye_D)
             
                u = int(imgpoints[0,0,0]* self.fisheye_image_width / self.fisheye_calib_width)
                v = int(imgpoints[0,0,1]* self.fisheye_image_height / self.fisheye_calib_height)

                # u and v are now the coordinate of the face in the coordinate of the face center in the fisheye frame
                # we only use the horizontal coordinate for matching, fish_loc_px

                fish_loc_px=u

                dist_closest_bb = self.MAXDISTANCE
                idx_closest_bb = None

                # for each face (fish_loc_px), compute its distance to all bodies roi
                # and select the closest for a match if the distance is not too large (200 pixels)

                # if case not trivial, we use an hungarian algorithm using scipi linear_sum_assignment
                # - build a matrix where each row is the distance of a given face to all the bodies, doesn't need to be square

                for (body_id,roi) in self.current_bodies_roi.items() :
                    dist = abs(roi.x_offset + roi.width/2 - fish_loc_px)
                    #print('  BODY : id {},  dist {}, roi : x {}, y {}, width {}, height {}, fish_loc_px {}'.format(
                    #    body_id[:-1], dist, roi.x_offset, roi.y_offset, roi.width, roi.height, fish_loc_px))
                    if dist < dist_closest_bb :
                        dist_closest_bb = dist
                        idx_closest_bb = body_id
                    distances_matrix_flat=np.append(distances_matrix_flat,dist)
                # deal with trivial case
                if dist_closest_bb < self.MAXDISTANCE and use_hungarian == False :
                    match_msg = IdsMatch()
                    match_msg.id1 = idx_closest_bb
                    match_msg.id1_type = IdsMatch.BODY
                    match_msg.id2 = face_id
                    match_msg.id2_type = IdsMatch.FACE
                    match_msg.confidence = self.compute_confidence(dist_closest_bb)
                    self.candidate_matches_publisher.publish(match_msg)
                    self.match_count += 1

                    #print('  current_faces {} | current_bodies {}'.format(list(self.current_faces.keys()), list(self.current_bodies.keys())))
                    #print('  current_faces_roi {} | current_bodies_roi {}'.format(list(self.current_faces_roi.keys()), list(self.current_bodies_roi.keys())))
                
                    if face_id in self.current_faces and idx_closest_bb in self.current_bodies and self.publish_matches :
                        image_match= cv2.vconcat([self.current_faces[str(face_id)], self.current_bodies[str(idx_closest_bb)]])
                        cv2.putText(image_match, str(dist_closest_bb), org=(10, 100), 
                            fontFace=cv2.FONT_HERSHEY_SIMPLEX,  fontScale=0.8, color=(0,255,0), 
                            thickness=self.line_thickness, lineType=cv2.LINE_AA)
                        if image_matches is None:
                            image_matches=image_match
                        else:
                            image_matches=cv2.hconcat([image_matches,image_match])
            
            if use_hungarian == True :
                distances_matrix=distances_matrix_flat.reshape(n_faces,n_bodies)
                #print(self.current_bodies_roi.items())
                #print(self.current_faces_roi.items())
                #print(distances_matrix)
                face_ind, body_ind = linear_sum_assignment(distances_matrix)

                for i in range(len(face_ind)):
                    match_msg = IdsMatch()
                    match_msg.id1 = list(self.current_bodies_roi.items())[body_ind[i]][0]
                    match_msg.id1_type = IdsMatch.BODY
                    match_msg.id2 = list(self.current_faces_roi.items())[face_ind[i]][0]
                    match_msg.id2_type = IdsMatch.FACE
                    match_msg.confidence = self.compute_confidence(distances_matrix[face_ind[i], body_ind[i]])
                    self.candidate_matches_publisher.publish(match_msg)
                    self.match_count += 1
                    if self.publish_matches :
                        image_match= cv2.vconcat([self.current_faces[str(list(self.current_faces_roi.items())[face_ind[i]][0])], 
                            self.current_bodies[str(list(self.current_bodies_roi.items())[body_ind[i]][0])]])
                        dist_closest_bb=distances_matrix[face_ind[i], body_ind[i]]
                        cv2.putText(image_match, str(dist_closest_bb), org=(10, 100), 
                            fontFace=cv2.FONT_HERSHEY_SIMPLEX,  fontScale=0.8, color=(0,255,0), 
                            thickness=self.line_thickness, lineType=cv2.LINE_AA)                           
                        if image_matches is None:
                            image_matches=image_match
                        else:
                            image_matches=cv2.hconcat([image_matches,image_match])

            #### Create CompressedImage ####
            if self.publish_matches and image_matches is not None: 
                image_matches = cv2.cvtColor(image_matches, cv2.COLOR_RGB2BGR)
                img_msg = CompressedImage()
                #img_msg.header = self.front_fisheye_camera_data.header
                img_msg.format = "jpeg"
                img_msg.data = np.array(cv2.imencode('.jpg', image_matches)[1]).tobytes()  #tostring()
                # img_msg = self.bridge.cv2_to_imgmsg(image, "rgb8")
                # img_msg.header = self.img_data.header
                self._head_body_matches_pub.publish(img_msg)


    def run(self):
        self.hri_listener.start()
        rate = rospy.Rate(10)
        while not rospy.is_shutdown():
            self.current_faces.clear()
            self.current_faces_roi.clear()
            with self.hri_listener.faces.lock :
                for face_id,face in self.hri_listener.faces.items() :
                    if face.roi and face.cropped :
                        self.current_faces_roi[face_id]=face.roi
                        self.current_faces[face_id] = cv2.resize(self.bridge.imgmsg_to_cv2(face.cropped, "bgr8"),(100,100))
            with self.hri_listener.bodies.lock :
                self.current_bodies.clear()
                self.current_bodies_roi.clear()
                for body_id,body in self.hri_listener.bodies.items() :
                    if body.roi and body.cropped :
                        self.current_bodies_roi[body_id]=body.roi
                        self.current_bodies[body_id] = cv2.resize(self.bridge.imgmsg_to_cv2(body.cropped, "bgr8"),(100,100))
            # print ("{} faces and {} bodies to match".format(len(self.current_faces_roi),len(self.current_bodies_roi)))
            if self.current_faces_roi and self.current_bodies_roi:
                self.compute_matches_geom()
            rate.sleep()


    def _diagnostics_callback(self, event):
        self.publish_diagnostics()


    def publish_diagnostics(self):
        diagnostic_msg = DiagnosticArray()
        diagnostic_msg.status = []
        diagnostic_msg.header.stamp = rospy.Time.now()
        status = DiagnosticStatus()
        status.name = 'Functionality: AV Perception: Face Body candidate matches'
        status.values.append(KeyValue(key='Number of tracked bodies', value='{}'.format(len(self.current_bodies_roi))))
        status.values.append(KeyValue(key='Number of tracked faces', value='{}'.format(len(self.current_faces_roi))))
        status.values.append(KeyValue(key='Number of face-body matchs', value='{}'.format(self.match_count)))

        # by default
        status.level = DiagnosticStatus.OK
        status.message = ''
        if not self.current_bodies_roi:
            status.level = DiagnosticStatus.OK
            status.message = 'No bodies tracked as input'
        if not self.current_faces_roi:
            status.level = DiagnosticStatus.OK
            status.message = 'No faces tracked as input'
        if self.current_bodies_roi and self.current_faces_roi and self.match_count == 0:
            status.level = DiagnosticStatus.WARN
            status.message = 'Was not able to match any tracked faces with any tracks from the tracked bodies'

        diagnostic_msg.status.append(status)
        self._diagnostics_pub.publish(diagnostic_msg)

    
    def close(self):
        if self._publishers:
            for publisher in self._publishers:
                if isinstance(publisher, dict):
                    for pub in publisher.values():
                        pub.unregister()
                else:
                    publisher.unregister()
        if self._timers:
            for timer in self._timers:
                timer.shutdown()


if __name__ == '__main__':
    matcher=FaceBodyMatcher()
    matcher.run()

    
