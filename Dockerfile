# to compile with : docker build -t facebodycandidatematches .
# to run with :
# docker run --rm -it --env ROS_MASTER_URI=http://192.168.0.10:11311 --env ROS_IP=192.168.0.1 -v /etc/hosts:/etc/hosts -v /local_scratch:/local_scratch facebodycandidatematches

# TODO: dockerfile needs to be adapted to the new project structure

FROM ros:noetic-perception-focal

RUN apt-get update && apt-get install -y --no-install-recommends \
    python3-pip python3-matplotlib python3-scipy\
    && rm -rf /var/lib/apt/lists/*


RUN mkdir -p /home/ros/facebodycandidatematches/src
WORKDIR /home/ros/facebodycandidatematches/

# ROS4HRI requires a more recent version of numpy than the focal's one
# cv2.sift is in opencv-contrib-python
RUN pip3 install -U numpy opencv-contrib-python 


# assumes that https://github.com/ros4hri/hri_msgs.git
# has been cloned in the project directory
RUN mkdir -p src/hri_msgs
COPY hri_msgs src/hri_msgs

# actual code of the facebodycandidatematches module
COPY src src/facebodycandidatematches

# Compile ros modules (mainly messages)
RUN /bin/bash -c "source /opt/ros/noetic/setup.bash && catkin_make"

#RUN mkdir -p src/spring_msgs
#ADD src/spring_msgs src/spring_msgs