#! /usr/bin/env python
from body_to_face_mapper import FaceBodyMatcher
import rospy

if __name__ == '__main__':
    rospy.init_node("facebodycandidatematches")
    node = FaceBodyMatcher()
    node.run()
    node.close()


